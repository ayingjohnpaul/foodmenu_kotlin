package com.example.foodmenukotlin.graphqlservices

import com.example.foodmenukotlin.model.User
import com.example.foodmenukotlin.repository.UserRepository
import io.leangen.graphql.annotations.GraphQLArgument
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.annotations.GraphQLQuery
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*


@Component
@GraphQLApi
class UserService {

    @Autowired
    var userRepository:UserRepository?=null


    @GraphQLQuery(name = "users", description = "list of users")
    fun getAllUsers():List<User>{
        return userRepository!!.findAll()
    }


    @GraphQLQuery(name = "user", description = "list of users")
    fun getUser(@GraphQLArgument(name = "id") id:UUID):User{
        return userRepository?.findById(id)!!.get()
    }


    @GraphQLMutation(name = "updateuser", description = "update user")
    fun updateUser(@GraphQLArgument(name = "id") id:UUID, @GraphQLArgument(name = "username") username: String ): User{

        var user = userRepository!!.findById(id).get()

        user.name =  username

       return userRepository?.save(user)!!

    }

}