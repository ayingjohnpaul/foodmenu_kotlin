package com.example.foodmenukotlin.payload

import org.hibernate.validator.constraints.Email
import org.hibernate.validator.constraints.NotBlank
import javax.validation.constraints.Size


class SignUpRequest {
    @NotBlank
    @Size(min = 4, max = 40)
    var name: String? = null

    @NotBlank
    @Size(min = 3, max = 15)
    var username: String? = null

    @NotBlank
    @Size(max = 40)
    @Email
    var email: String? = null

    @NotBlank
    @Size(min = 6, max = 20)
    var password: String? = null
}
