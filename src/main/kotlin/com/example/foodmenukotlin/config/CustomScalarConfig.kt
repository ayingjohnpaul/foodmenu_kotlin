package com.example.foodmenukotlin.config

import graphql.language.StringValue
import graphql.schema.Coercing
import graphql.schema.GraphQLScalarType
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import java.util.*

@Configuration
class CustomScalarConfig: GraphQLScalarType("UUID", "UUID", object : Coercing<UUID, String> {

    override fun serialize(input: Any): String? = when (input) {
        is String -> input
        is UUID -> input.toString()
        else -> null
    }

    override fun parseValue(input: Any): UUID? = parseLiteral(input)

    override fun parseLiteral(input: Any): UUID? = when (input) {
        is StringValue -> UUID.fromString(input.value)
        else -> null
    }
})
