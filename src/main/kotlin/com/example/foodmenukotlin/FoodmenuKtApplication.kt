package com.example.foodmenukotlin

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters

import javax.annotation.PostConstruct
import org.springframework.core.Ordered
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import java.util.Collections.singletonList
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import java.util.*


@SpringBootApplication
@EntityScan(basePackageClasses = arrayOf(FoodmenuKtApplication::class, Jsr310JpaConverters::class))
class FoodmenuKtApplication {

    @PostConstruct
    internal fun init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(FoodmenuKtApplication::class.java, *args)
        }
    }

    @Bean
    fun simpleCorsFilter(): FilterRegistrationBean<CorsFilter> {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()
        config.allowCredentials = true
        config.allowedOrigins = Arrays.asList("http://localhost:3000")
        config.allowedMethods = Collections.singletonList("*")
        config.allowedHeaders = Collections.singletonList("*")
        config.maxAge = 3600
        source.registerCorsConfiguration("/**", config)
        val bean = FilterRegistrationBean<CorsFilter>(CorsFilter(source))
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE)
        return bean
    }
}
